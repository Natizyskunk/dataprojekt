# Changelog
Ce fichier liste l'ensemble des modifications apportées à l'application Dataprojekt.

---

## 19/03/2023
* Develop : Modification (Surchargement) de la page d'erreur 404.
* Develop : Création de la commande de génération d'un nouvel administrateur.
* Develop : Gestion des droits d'acces.
* Develop : Centrage CSS pour la page d'enregistrement et celle de connexion.
* Develop : Update + Fix Syntax.


## 18/03/2023
* Develop : Update de la page d'erreur 404.
* Develop : Update des livres et de la homepage.
* Develop : Update du CSS pour les e-mails.
* Develop : Update de la configuration de base du projet.
* Develop : Ajout de symfony/rate-lmiter.

## 17/03/2023
* Develop : Ajout de 'cratedAt', 'updatedAt' et 'connectedAt' dans l'entité User afin de connaite la date de création et de dernière connexion d'un utilisateur.
* Develop : Update des API.
* Develop : Update de la page d'erreur 404.
* Develop : Update de la page de connexion.
* Develop : Ajout et update des template de base + layout.
* Develop : Update de l'entité User.
* Develop : Update des pages Registration.
* Develop : Update de l'entité ResetPassword.
* Develop : Update de l'entité Book.

## 16/03/2023
* Develop : Ajout extensions Twig custom.
* Develop : MAJ des composants vue.
* Develop : Update + création des assets.
* Develop : Update de la config.
* Develop : Update + fix syntax.
* Develop : Remove old files.
* Develop : Update book templates + vues.
* Develop : Update Javascript.

## 15/03/2023
* Develop : Ajout de l'entité User.
* Develop : Update du layout du header et du footer.
* Develop : Suppression de fichiers inutiles.
* Develop : Ajout de book.
* Develop : Update CSS et JS.
* Develop : Ajout de Boostrap 5.
* Develop : Ajout du CRUD pour l'entité Book.
* Develop : Ajout de l'entité Book.
* Develop : Ajout de la fonction de réinitialisation du mot de passe (mot de passe oublié).

## 14/03/2023
* Develop : Gestion de l'affichage des messages de succes et d'erreurs.
* Develop : Refonte complète du système d'enregistrement et de connexion.
* Develop : Structure de base controllers + templates + etc...
* Develop : Update des assets.
* Develop : Update des scripts utilitaires.
* Develop : Update Config Symfony + VueJS.
* Develop : Ajout des scripts utilitaires.
* Develop : Structure de base controllers + templates.
* Develop : Config Symfony + VueJS.
* Develop : Initial commit.

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import { registerVueControllerComponents } from '@symfony/ux-vue';

// any CSS you import will output into a single css file (app.css in this case)
import './../styles/app.scss';

// start the Stimulus application
import './bootstrap';

// Registers Vue.js controller components to allow loading them from Twig
registerVueControllerComponents(require.context(
    './../vue/controllers',
    true,
    /\.vue$/
));

// If you prefer to lazy-load your Vue.js controller components, you can use the following line instead:
//registerVueControllerComponents(require.context('./vue/controllers', true, /\.vue$/, 'lazy'));

import axios from 'axios';
import promise from 'promise';

/**
 * Get the basic informations from the API.
 *
 */
export function fetchApi() {
    return new promise((resolve, reject) => {
        axios.get(`/api`)
            .then((res) => resolve(res))
            .catch((err) => reject(err))
    });

    /* return new promise((resolve, reject) => {
        fetch("/api", {"method": "GET"})
        .then(response => response.json())
        .then((result) => result['app.name']);
    }); */
}

/**
 * Get the variable from the dom.
 *
 * @param {string} variable
 *
 * @return {string} window.var
 */
export function getVar(variable) {
    return window.var == variable ? window.var : null;
}

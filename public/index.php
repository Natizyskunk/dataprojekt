<?php

use App\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

/* function getEnv(string $prefix, string $name, \Closure $getEnv) {
    $env = $getEnv($name);
    return strtolower($env);
} */

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}

return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};

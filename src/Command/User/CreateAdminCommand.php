<?php
namespace App\Command\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminCommand extends Command
{
    //The command name (the part after "bin/console")
    // Also shown when running "php bin/console list"
    protected static $defaultName = 'app:create-admin';

    // The command description shown when running "php bin/console list"
    protected static $defaultDescription = 'Permet de créer un nouvel administrateur.';

    /**
     * @var bool
     */
    private $requireUsername;

    /**
     * @var bool
     */
    private $requirePassword;

    /**
     * @var UserPasswordHasherInterface
     */
    private $userPasswordHasher;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Constructeur
     *
     * @param bool                          $requireUsername
     * @param bool                          $requirePassword
     * @param UserPasswordHasherInterface   $userPasswordHasher
     * @param EntityManagerInterface        $entityManager
     */
    public function __construct(bool $requireUsername = true, bool $requirePassword = true, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager)
    {
        $this->requireUsername = $requireUsername;
        $this->requirePassword = $requirePassword;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:create-administrator')
            ->setProcessTitle("Création d'un nouvel administrateur")
            ->setDescription('Permet de créer un nouvel administrateur.')
            ->setHelp("
                Cette commande vous permets de créer un nouvel administrateur... \n
                Pour l'utiliser, il vous suffit de passer en arguments l'adresse e-mail et le mot de passe. \n
                Exemple : app:create-admin admin@dataprojekt.com superuser
            ")
            ->setAliases([
                'app:create-admin',
                'app:add-administrator',
                'app:add-admin'
                ])
            ->addArgument('username', $this->requireUsername ? InputArgument::REQUIRED : InputArgument::OPTIONAL, "Adresse e-mail de l'administrateur")
            ->addArgument('password', $this->requirePassword ? InputArgument::REQUIRED : InputArgument::OPTIONAL, "Mot de passe de l'administrateur")
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('Cette commande accepte uniquement une instance de "ConsoleOutputInterface".');
        }

        $section = $output->section();

        $section->writeln([
            "Création d'un nouvel administrateur",
            '============',
            '',
        ]);

        $section->writeln('Vous êtes sur le point de créer un nouvel utilisateur avec le rôle "ROLE_ADMIN" !');

        if ($input->getArgument('username') && $input->getArgument('password')) {
            $user = new User();
            $user->setEmail($input->getArgument('username'));
            $user->setRoles(['ROLE_ADMIN']);
            $user->setPassword($this->userPasswordHasher->hashPassword($user, $input->getArgument('password')));
            $user->setFirstname('Administrateur');
            $user->setLastname('Administrateur');
            $user->setIsVerified(true);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $section->writeln('Le nouvel administrateur: '.$input->getArgument('username').' a bien été créé.');
            $section->writeln('Au revoir.');
            return Command::SUCCESS;
        }
        else if (!$input->getArgument('username')) {
            $section->writeln("Merci de renseigner l'adresse e-mail de l'administrateur à créer.");

            $section->writeln('Au revoir');
            return Command::INVALID;
        }
        else if ($input->getArgument('username') && !$input->getArgument('password')) {
            $section->writeln("Merci de renseigner le mot de passe de l'administrateur à créer.");

            $section->writeln('Au revoir');
            // $section->overwrite('Au revoir');
            return Command::INVALID;
        }
        else {
            $section->writeln("Une erreur est apparue lors de la création du nouvel administrateur.");
            $section->writeln("Merci de réessayer. Si le problème persiste, merci de contacter votre service informatique.");

            $section->writeln('Au revoir');
            // $section->overwrite('Au revoir');
            return Command::FAILURE;
        }
    }
}

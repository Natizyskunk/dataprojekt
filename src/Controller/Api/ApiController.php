<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends AppController
{
    /**
     * @param ContainerBagInterface $params
     * @return Response|JsonResponse Response.
     *
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(ContainerBagInterface $params): Response
    {
        return new JsonResponse([
            'app.name' => $params->get('app.name'),
        ]);
    }
}

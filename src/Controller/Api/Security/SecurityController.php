<?php
namespace App\Controller\Api\Security;

use App\Controller\AppController;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

/**
 * @Route("/api/login", name="api_security_")
 */
class SecurityController extends AppController
{
    /**
     * @param ContainerBagInterface $params
     * @return Response|JsonResponse Response.
     *
     * @Route("/", name="login", methods={"GET"})
     */
    public function index(?User $user): Response
    {
        if (null === $user) {
            return $this->json([
                'message' => 'missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }
        $token = ''; // somehow create an API token for $user

        return $this->json([
            'user'  => $user->getUserIdentifier(),
            'token' => $token,
        ]);
    }
}

<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AppController extends AbstractController
{
    /**
     * Retourne la valeur du paramètre recherché
     *
     * @param String $param Parameter name
     *
     * @return array|bool|float|int|string|\UnitEnum|null
     */
    public function getParam(String $param)
    {
        return $this->getParameter($param);
    }

    /**
     * Retourne la langue (locale) de l'application
     *
     * @return String Application language (locale)
     */
    public function getLocale(): String
    {
        return $this->getParam('locale');
    }

    /**
     * Retourne le nom de l'application
     *
     * @return String Application name
     */
    public function getAppName(): String
    {
        return $this->getParam('app.name');
    }

    /**
     * Retourne l'environnement de l'application
     *
     * @return String Application environment
     */
    public function getAppEnv(): String
    {
        return $this->getParam('app.environment');
    }
}

<?php

namespace App\Controller\Book;

use App\Controller\AppController;
use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/book", name="app_book_")
 * @IsGranted("ROLE_USER")
 */
class BookController extends AppController
{
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var Security
     */
    private $security;

    /**
     * Constructeur
     *
     * @param ParameterBagInterface     $params
     * @param Security                  $security
     */
    public function __construct(ParameterBagInterface $params, Security $security)
    {
        $this->params = $params;
        $this->security = $security;
    }

    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     */
    public function new(Request $request, BookRepository $bookRepository): Response
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $book->setOwner($this->getUser());
            $bookRepository->add($book, true);

            return $this->redirectToRoute($this->params->get('app.main_route'), [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book/new.html.twig', [
            'book' => $book,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Book $book): Response
    {
        $bookInfos = [
            'id' => $book->getId(),
            'title' => $book->getTitle(),
            'owner' => $book->getOwner()->getFirstname().' '.$book->getOwner()->getLastname(),
            'isEditable' =>  ($this->security->isGranted('ROLE_ADMIN') || $book->getOwner() == $this->getUser()) ? true : false
        ];

        return $this->render('book/show.html.twig', [
            'book' => $bookInfos
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Book $book, BookRepository $bookRepository): Response
    {
        if ($this->security->isGranted('ROLE_ADMIN') || $book->getOwner() !== $this->getUser()) {
            throw $this->createAccessDeniedException("Vous n'avez pas les droits pour éditer ce livre.");
        }

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bookRepository->add($book, true);

            return $this->redirectToRoute($this->params->get('app.main_route'), [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book/edit.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
            'isEditable' =>  ($this->security->isGranted('ROLE_ADMIN') ||$book->getOwner() == $this->getUser()) ? true : false
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"POST"})
     */
    public function delete(Request $request, Book $book, BookRepository $bookRepository): Response
    {
        if ($this->security->isGranted('ROLE_ADMIN') || $book->getOwner() !== $this->getUser()) {
            throw $this->createAccessDeniedException("Vous n'avez pas les droits pour supprimer ce livre.");
        }

        if ($this->isCsrfTokenValid('delete'.$book->getId(), $request->request->get('_token'))) {
            $bookRepository->remove($book, true);
        }

        return $this->redirectToRoute($this->params->get('app.main_route'), [], Response::HTTP_SEE_OTHER);
    }
}

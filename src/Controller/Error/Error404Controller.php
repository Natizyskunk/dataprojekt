<?php
namespace App\Controller\Error;

use App\Controller\AppController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Twig\AppVariable;

/**
 * @Route("/error-404", name="app_error_error404_")
 */
class Error404Controller extends AppController
{
    /**
     * @return Response Response.
     *
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('error/error-404.html.twig');
    }
}

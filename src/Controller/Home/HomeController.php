<?php
namespace App\Controller\Home;

use App\Controller\AppController;
use App\Entity\Book;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/", name="app_home_")
 */
class HomeController extends AppController
{
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var Security
     */
    private $security;

    /**
     * Constructeur
     *
     * @param UserRepository            $userRepository
     * @param ParameterBagInterface     $params
     * @param Security                  $security
     */
    public function __construct(ParameterBagInterface $params, Security $security)
    {
        $this->params = $params;
        $this->security = $security;
    }

    /**
     * @return Response Response.
     *
     * @Route("/", name="index")
     */
    public function index(BookRepository $bookRepository): Response
    {
        $user = [];

        if ($this->getUser()) {
            $user = [
                'id' => $this->getUser()->getId(),
                'email' => $this->getUser()->getEmail(),
                'roles' => $this->getUser()->getRoles() ? json_encode($this->getUser()->getRoles()) : '',
                'firstname' => $this->getUser()->getFirstname(),
                'lastname' => $this->getUser()->getLastname()
            ];
        }

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $books = $bookRepository->findAll();
        } else {
            $books = $bookRepository->findBy(['owner' => $this->getUser()]);
        }

        $bookList = [];
        foreach ($books as $book) {
            $bookList[] = [
                'id' => $book->getId(),
                'title' => $book->getTitle(),
                'owner' => $book->getOwner()->getFirstname().' '.$book->getOwner()->getLastname(),
                'isEditable' => ($this->security->isGranted('ROLE_ADMIN') ||$book->getOwner() == $this->getUser()) ? true : false
            ];
        }

        return $this->render('home/index.html.twig', [
            'appName' => $this->getAppName(),
            'user' => $user,
            'books' => $bookList
        ]);
    }
}

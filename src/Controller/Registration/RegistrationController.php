<?php

namespace App\Controller\Registration;

use App\Controller\AppController;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use App\Security\UserAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

/**
 * @Route("/", name="app_register_")
 */
class RegistrationController extends AppController
{
    private $emailVerifier;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * Constructeur
     *
     * @param EmailVerifier         $emailVerifier
     * @param ParameterBagInterface $params
     */
    public function __construct(EmailVerifier $emailVerifier, ParameterBagInterface $params)
    {
        $this->emailVerifier = $emailVerifier;
        $this->params = $params;
    }

    /**
     * @Route("/register", name="index")
     */
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute($this->params->get('app.main_route'));
        }

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        $message = '';

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('app_register_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address($this->params->get('app.email.default_from'), $this->params->get('app.email.default_from_name')))
                    ->to($user->getEmail())
                    ->subject("Merci d'activer votre compte")
                    ->htmlTemplate('registration/confirmation-email.html.twig')
            );

            // $message = "Un e-mail vient d'être envoyé contenant un lien que vous pouvez utiliser pour activer votre compte.";
            return $this->redirectToRoute('app_register_check_email');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'message' => $message
        ]);
    }

    /**
     * Confirmation page after a user has requested a password reset.
     *
     * @Route("/register/check-email", name="check_email")
     */
    public function checkEmail(): Response
    {
        return $this->render('registration/check-email.html.twig');
    }

    /**
     * @Route("/verify/email", name="verify_email")
     */
    public function verifyUserEmail(Request $request, TranslatorInterface $translator, UserRepository $userRepository, UserAuthenticatorInterface $userAuthenticator, UserAuthenticator $authenticator): Response
    {
        $id = $request->get('id');

        if (null === $id) {
            return $this->redirectToRoute($this->params->get('app.register_route'));
        }

        $user = $userRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute($this->params->get('app.register_route'));
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute($this->params->get('app.register_route'));
        }

        $this->addFlash('verify_email_success', 'Votre compte a bien été activé.');

        return $userAuthenticator->authenticateUser($user, $authenticator, $request);
    }
}

<?php

namespace App\Controller\Security;

use App\Controller\AppController;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AppController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * Constructeur
     *
     * @param UserRepository            $userRepository
     * @param ParameterBagInterface     $params
     */
    public function __construct(UserRepository $userRepository, ParameterBagInterface $params)
    {
        $this->userRepository = $userRepository;
        $this->params = $params;
    }

    /**
     * @Route("/login", name="app_security_login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        $message = null;
        $error = null;

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'message' => $message
        ]);
    }

    /**
     * @Route("/logout", name="app_security_logout")
     * @IsGranted("ROLE_USER")
     */
    public function logout(): void
    {
        throw new \LogicException("Cette méthode peut être vide - Elle sera intercepté par la clé 'logout' de votre firewall.");
    }
}

<?php

namespace App\EventListener;

use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class RedirectUserListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorageInterface;

    /**
     * @var RouterInterface
     */
    private $routerInterface;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * Constructeur
     *
     * @param TokenStorageInterface $tokenStorageInterface
     * @param RouterInterface       $routerInterface
     * @param ParameterBagInterface $params
     */
    public function __construct(TokenStorageInterface $tokenStorageInterface, RouterInterface $routerInterface, ParameterBagInterface $params)
    {
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->routerInterface = $routerInterface;
        $this->params = $params;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }

    /**
     * Au moment de la requête du Kernel.
     *
     * @param RequestEvent $event
     *
     * @return void
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if ($event->isMainRequest()) {
            if ($this->isUserLogged()) {
                $currentRoute = $request->attributes->get('_route');

                if ($this->isAuthenticatedUserOnAnonymousPage($currentRoute)) {
                    $response = new RedirectResponse($this->routerInterface->generate($this->params->get('app.main_route')));
                    $event->setResponse($response);
                }
            }
        }
    }

    /**
     * Test si l'utilisateur est authentifié.
     *
     * @return User|null
     */
    private function isUserLogged(): ?User
    {
        $user = $this->tokenStorageInterface->getToken() !== null ? $this->tokenStorageInterface->getToken()->getUser() : null;
        return $user instanceof User ? $user : null;
    }

    /**
     * Test si l'utilisateur authentifié se trouve sur une page accessible anonymement.
     *
     * @param mixed $currentRoute Route courante
     *
     * @return bool
     */
    private function isAuthenticatedUserOnAnonymousPage($currentRoute)
    {
        $anonymousPages = [
            'register',
            'app_register_index',
            'app_register_verify_email',
            'app_security_login',
            'app_reset_password_request',
            'app_reset_password_check_email',
            'app_reset_password_reset'
        ];

        return in_array(
            $currentRoute,
            $anonymousPages
        );
    }
}

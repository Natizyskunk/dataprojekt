<?php

namespace App\EventSubscriber;

use App\Entity\User;
use \Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\UserPassportInterface;
use Symfony\Component\Security\Http\Event\CheckPassportEvent;

class CheckVerifiedUserSubscriber implements EventSubscriberInterface
{
    /**
     * Au moment de la vérification du Passport.
     *
     * @param CheckPassportEvent $event
     *
     * @return void
     */
    public function onCheckPassport(CheckPassportEvent $event)
    {
        $passport = $event->getPassport();

        if (!$passport instanceof UserPassportInterface) {
            throw new Exception('Type de passport inattendu.');
        }

        $user = $passport->getUser();

        if (!$user instanceof User) {
            throw new Exception("Type d'utilisateur inattendu.");
        }

        if (!$user->IsVerified()) {
            throw new CustomUserMessageAuthenticationException(
                "Merci de vérifier votre compte via le lien d'activation qui vous a été envoyé par e-mail avant de pouvoir vous connecter."
            );
        }
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            CheckPassportEvent::class => ['onCheckPassport', -10],
        ];
    }
}

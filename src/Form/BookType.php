<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => "Titre du livre",
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => null
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer le titre du livre"
                    ]),
                    new Length([
                        'min' => 2,
                        'minMessage' => 'Le titre de votre livre doit avoir une longueur minimum de {{ limit }} caractères',
                        'max' => 4096
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class
        ]);
    }
}

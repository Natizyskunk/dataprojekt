<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Adresse e-mail',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'email',
                    'autofocus' => null
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer votre adresse e-mail"
                    ]),
                ]
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom ',
                'attr' => [
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer votre prénom"
                    ]),
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom de famille ',
                'attr' => [
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer votre nom de famille"
                    ]),
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options' => [
                    'attr' => [
                        'class' => 'form-control password-field',
                        'autocomplete' => 'new-password'
                    ],
                    // 'constraints' => new UserPassword([
                    //     'message' => 'Entrez un mot de passe valide et réessayez.'
                    // ]),
                    'constraints' => [
                        new NotBlank([
                            'message' => "Merci d'entrer un mot de passe"
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Votre mot de passe doit avoir une longueur minimum de {{ limit }} caractères',
                            'max' => 4096
                        ]),
                    ],
                    'label' => 'Mot de passe'
                ],
                'second_options' => [
                    'attr' => [
                        'class' => 'form-control password-field',
                        'autocomplete' => 'new-password'
                    ],
                    'label' => 'Confirmez le mot de passe'
                ],
                'invalid_message' => 'Les champs mot de passe doivent correspondre.',
                'mapped' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}

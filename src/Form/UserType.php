<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $options['data'];

        $builder
            ->add('email', EmailType::class, [
                'label' => 'Adresse e-mail',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'email',
                    'autofocus' => null
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer votre adresse e-mail"
                    ]),
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'ROLE_USER' => 'ROLE_USER',
                    'ROLE_ADMIN' => 'ROLE_ADMIN'
                ],
                'label' => 'Rôles',
                'required' => true,
                'attr' => [
                    'class' => 'form-control'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci de sélectionner au minimum un rôle"
                    ]),
                ]
            ])
        ;

        if ($user->getId() === null) {
            $builder
                ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'required' => true,
                    'first_options' => [
                        'attr' => [
                            'class' => 'form-control password-field',
                            'autocomplete' => 'new-password'
                        ],
                        // 'constraints' => new UserPassword([
                        //     'message' => 'Entrez un mot de passe valide et réessayez.'
                        // ]),
                        'constraints' => [
                            new NotBlank([
                                'message' => "Merci d'entrer un mot de passe"
                            ]),
                            new Length([
                                'min' => 6,
                                'minMessage' => 'Votre mot de passe doit avoir une longueur minimum de {{ limit }} caractères',
                                'max' => 4096
                            ])
                        ],
                        'label' => 'Mot de passe'
                    ],
                    'second_options' => [
                        'attr' => [
                            'class' => 'form-control password-field',
                            'autocomplete' => 'new-password'
                        ],
                        'label' => 'Confirmez le mot de passe'
                    ],
                    'invalid_message' => 'Les champs mot de passe doivent correspondre.',
                    'mapped' => false
                ])
            ;
        }

        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Prénom ',
                'attr' => [
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer votre prénom"
                    ]),
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom de famille ',
                'attr' => [
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => "Merci d'entrer votre nom de famille"
                    ]),
                ]
            ])
            ->add('isVerified', CheckboxType::class, [
                'label'    => 'Compte activé',
                'required' => false,
            ])
        ;

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    return implode(', ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    return explode(', ', $tagsAsString);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}

#!/usr/bin/env bash

echo "Suppression de la base de données"
php bin/console doctrine:database:drop --force

echo "Creation de la base de données"
php bin/console doctrine:database:create

echo "Création des migrations"
php bin/console make:migration

echo "Exécution des migrations"
php bin/console doctrine:migrations:migrate --no-interaction

echo "Mise à jour du schéma"
php bin/console doctrine:schema:update --force --complete

# echo "Initialisation de l'application"
# php bin/console app:init

# echo "Initialisation des données"
# php bin/console app:init:utilisateurs

echo "Compilation des assets via webpack encore"
npm run dev

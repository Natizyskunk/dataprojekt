#!/usr/bin/env bash

echo "Installation des dépendances Composer"
composer install

echo "Installation des dépendances NPM"
npm install

echo "Création des migrations"
php bin/console make:migration

echo "Exécution des migrations"
php bin/console doctrine:migrations:migrate --no-interaction

echo "Mise à jour du schéma"
php bin/console doctrine:schema:update --force --complete

echo "Compilation des assets via webpack encore"
npm run dev

echo "Vidange du cache de Babel"
rm -rf node_modules/.cache/babel-loader/

echo "Vidange du cache de Symfony"
php bin/console cache:clear
